
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <link rel="stylesheet" href="style.css">
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <br>
    <img src="background.jpg" class="backgr">
    <div class="content">
      <p class="head1"> Skills </p>

      <div class="skillcontent">

        <p class="head"> SINGING </p>
        <div class="define">
          <p>
            Most of the time when I get bored,I sing most of my favorites songs.
            I used to sing in the school and church before,when I was a choir member.
            I also love to sing in karaoke with my family, it is also our tradition to
            sing everytime their is an occassion in our family.
          </p>

        </div>

           <p class="head"> DANCING </p>
           <div class="define">
             <p> Dancing is one of my favorite things to do.
                 I used to dance when I was in high school
                 I'm a member of a dance troupe and we used to dance
                 and compete with others. <br>
             </p>

           </div>

           <p class="head"> FLUTE </p>
           <div class="define">
             <p> I can play flute even though I'm not really good at it.
                When I was young I used to play flute in the church every sunday before.
                My aunt teach me how to play it, and she is also the one who encourage
                me to play in the church.
             </p>

           </div>

      </div>

    </div>
  </body>
</html>
