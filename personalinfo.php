<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="style.css">
    <title>Personal Information</title>
  </head>
  <body>
    <img src="background.jpg" class="backgr">
    <div class="content">
  <div class="personaltext">
    <p class="text"> Personal Information </p>
  </div>



  <div class="personcontent">
      <p class="myinfo"> NAME</p>
      <div class="txt">
        <p class="kim"> Kimberly A. Devocion</p>
      <div class="addr">
        <p class="add"> ADDRESS  </p>
        <div class="locate">
          <p class="area"> Quimpo Blvrd,Davao C.
          </p>
          <div class="bday">
            <p class="myday"> BIRTHDATE</p>
            <div class="day">
              <p class="date"> August 21, 1998</p>
              <div class="gend">
                <p class="gender"> GENDER </p>
                <div class="fmale">
                  <p class="female"> Female </p>
                  <div class="edad">
                    <p class="age"> AGE </p>
                    <div class="pila">
                      <p class="ages"> 20 </p>
                      <div class="heights">
                        <p class="height"> HEIGHT </p>
                        <div class="taas">
                          <p class="tangkad"> 5'0 </p>
                          <div class="weights">
                            <p class="weight"> WEIGHT </p>
                            <div class="bigat">
                              <p class="kilo"> 45 kg </p>
                              <div class="stat">
                                <p class="status"> STATUS</p>
                                <div class="taken">
                                  <p class="single"> Single</p>
                                  <div class="citi">
                                    <p class="citizenship"> CITIZENSHIP </p>
                                    <div class="fil">
                                      <p class="filipino"> Filipino</p>
                                      <div class="rel">
                                        <p class="religion"> RELIGION</p>
                                        <div class="cath">
                                          <p class="catholic"> Catholic </p>

                                        </div>

                                      </div>

                                    </div>

                                  </div>

                                </div>

                              </div>

                            </div>

                          </div>

                        </div>

                      </div>

                    </div>

                  </div>

                </div>

              </div>

            </div>

          </div>

        </div>

      </div>

      </div>

    </div>

  </div>


  </body>
</html>
