<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <link rel="stylesheet" href="style.css">
    <meta charset="utf-8">
    <title>Education</title>
  </head>
  <body>
    <br>
    <img src="background.jpg" class="backgr">
    <div class="content">
      <p class="text"> Educational Background </p>


      <div class="educationcontent">

        <p class="head"> TERTIARY </p>
        <div class="chuchu">
          <p> University of Southeastern Philippines <br>
              2015-present
          </p>
        </div>

              <p class="head"> SECONDARY</p>
              <div class="chuchu">
                <p> Daniel R. Aguinaldo National High School <br>
                    2014-2015
                </p>
              </div>

              <p class="head"> PRIMARY </p>
              <div class="chuchu">
                <p> Bolton Elementary School <br>
                    2010-2011
                </p>
              </div>
        </div>
     </div>
  </body>
</html>
