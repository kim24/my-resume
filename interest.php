<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <link rel="stylesheet" href="style.css">
    <meta charset="utf-8">
    <title>Interest</title>
  </head>
  <body>

    
    <br>
    <img src="background.jpg" class="backgr">
    <div class="content">
    <p class="head1"> Interest</p>

    <div class="interestcontent">

      <p class="head"> MOVIES </p>
        <div class="define">
          <p> Watching movies is part of my daily routine.
              I love to watch horror thrilling movies or movies that can make me
              scream.  I prefer to watch movies alone with lots of food.

           </p>

        </div>

           <p class="head"> MUSIC</p>
           <div class="define">
             <p> I have lots of favorite songs, singers and bands.
                 That's why music is part of my life. It is also my stress reliever
                 when ever I had a bad day, or when I'm bored. Earphone is my buddy
                 wherever I go I will not forget to bring it.
              </p>

           </div>

           <p class="head"> TRAVELLING </p>
           <div class="define">
             <p> I really love to go in different and beautiful places
                 I'm interested in visiting different places both inside
                 and outside of the philippines. I like to go in different beaches
                 and historical places.
            </p>

           </div>


    </div>

    </div>

  </body>
</html>
